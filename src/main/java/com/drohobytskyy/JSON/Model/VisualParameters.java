package com.drohobytskyy.JSON.Model;

public class VisualParameters {
    private String colour;
    private int wayOfCutting;
    private int transparency;

    public VisualParameters() {
    }

    public VisualParameters(String colour, int wayOfCutting, int transparency) {
        this.colour = colour;
        this.wayOfCutting = wayOfCutting;
        this.transparency = transparency;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public int getWayOfCutting() {
        return wayOfCutting;
    }

    public void setWayOfCutting(int wayOfCutting) {
        this.wayOfCutting = wayOfCutting;
    }

    public int getTransparency() {
        return transparency;
    }

    public void setTransparency(int transparency) {
        this.transparency = transparency;
    }

    @Override
    public String toString() {
        return "VisualParameters{" +
                "colour='" + colour + '\'' +
                ", wayOfCutting=" + wayOfCutting +
                ", transparency=" + transparency +
                '}';
    }
}
