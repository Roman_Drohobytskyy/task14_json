package com.drohobytskyy.JSON.View;

import java.io.IOException;

@FunctionalInterface
public interface Printable {

    void print() throws IOException;
}

