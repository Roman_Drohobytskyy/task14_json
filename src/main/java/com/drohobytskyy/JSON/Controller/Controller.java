package com.drohobytskyy.JSON.Controller;

import com.drohobytskyy.JSON.View.ConsoleView;

import java.io.IOException;

public class Controller {
    private final static String DIVIDER = "----------------------------------------------------------------------" +
            "-----------------------------------------------------------------";
    private ConsoleView view;

    public Controller(ConsoleView view) throws IOException {
        this.view = view;
        view.controller = this;
        view.createMenu();
        view.executeMenu(view.menu);
    }

    public void showSynchronizedBySingleLocker() {
        view.logger.warn(DIVIDER);
    }
}
