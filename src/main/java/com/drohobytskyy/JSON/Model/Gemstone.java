package com.drohobytskyy.JSON.Model;

public class Gemstone {
    private int gemstoneNo;
    private String name;
    private boolean preciousness;
    private String origin;
    private double value;
    private VisualParameters visualParams;

    public Gemstone() {
    }

    public Gemstone(int gemstoneNo, String name, boolean preciousness, String origin, double value,
                    VisualParameters visualParams) {
        this.gemstoneNo = gemstoneNo;
        this.name = name;
        this.preciousness = preciousness;
        this.origin = origin;
        this.value = value;
        this.visualParams = visualParams;
    }

    public int getGemstoneNo() {
        return gemstoneNo;
    }

    public void setGemstoneNo(int gemstoneNo) {
        this.gemstoneNo = gemstoneNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isPreciousness() {
        return preciousness;
    }

    public void setPreciousness(boolean preciousness) {
        this.preciousness = preciousness;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public VisualParameters getVisualParams() {
        return visualParams;
    }

    public void setVisualParams(VisualParameters visualParams) {
        this.visualParams = visualParams;
    }

    @Override
    public String toString() {
        return "Gemstone{" +
                "gemstoneNo=" + gemstoneNo +
                ", name='" + name + '\'' +
                ", preciousness=" + preciousness +
                ", origin='" + origin + '\'' +
                ", value=" + value +
                ", visualParams=" + visualParams +
                '}';
    }
}
