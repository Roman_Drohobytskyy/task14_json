package com.drohobytskyy.JSON;

import com.drohobytskyy.JSON.Controller.Controller;
import com.drohobytskyy.JSON.View.ConsoleView;

import java.io.IOException;

public class StartPoint {

    public static void main(String[] args) {
        {
            try {
                Controller controller = new Controller(new ConsoleView());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
